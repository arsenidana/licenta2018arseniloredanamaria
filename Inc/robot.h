/*
 * robot.h
 *
 */

#ifndef ROBOT_H_
#define ROBOT_H_
#include <stdint.h>

extern void RobotInitialization(void);
extern void RobotDisplayData(void);
extern void RobotControlAlgorithm(void);
extern void RobotPIDLoop(void);
extern void RobotStartStop(void);



#endif /* ROBOT_H_ */
