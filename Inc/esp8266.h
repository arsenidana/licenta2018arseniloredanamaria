/*
 * esp8266.h
 *
 *  Created on: 21 ian. 2017
 *      Author: Adrian
 */

#ifndef ESP8266_H_
#define ESP8266_H_
#include<stdint.h>

#define ESP8266_OK   1u
#define ESP8266_NOK  0u

#define ESP8266_REQ_HTTP_DATA     1
#define ESP8266_REQ_XML_DATA      0




extern uint8_t ESP8266Initialization(void);
extern void ESP8266RxCallBack(void);
extern uint8_t ESP8266TxCmd(uint8_t* command);
extern uint8_t ESP8266TxWeb(uint8_t* page,uint8_t type);
extern char *ESPItoa(int32_t Value, uint8_t Base);
extern void ESP8266MainCallBack(void);
extern volatile uint8_t rx_buf[512];

#endif /* ESP8266_H_ */
