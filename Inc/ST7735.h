#ifndef ST7735_H_
#define ST7735_H_
#include<stdint.h>

#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define ORANGE	0xFD20

#define NOP     0x00
#define SWRESET 0x01
#define RDDID   0x04
#define RDDST   0x09
#define SLPIN   0x10
#define SLPOUT  0x11
#define PTLON   0x12
#define NORON   0x13
#define INVOFF  0x20
#define INVON   0x21
#define DISPOFF 0x28
#define DISPON  0x29
#define CASET   0x2A
#define RASET   0x2B
#define RAMWR   0x2C
#define RAMRD   0x2E
#define PTLAR   0x30
#define COLMOD  0x3A
#define MADCTL  0x36
#define FRMCTR1 0xB1
#define FRMCTR2 0xB2
#define FRMCTR3 0xB3
#define INVCTR  0xB4
#define DISSET5 0xB6
#define PWCTR1  0xC0
#define PWCTR2  0xC1
#define PWCTR3  0xC2
#define PWCTR4  0xC3
#define PWCTR5  0xC4
#define VMCTR1  0xC5
#define RDID1   0xDA
#define RDID2   0xDB
#define RDID3   0xDC
#define RDID4   0xDD
#define GMCTRP1 0xE0
#define GMCTRN1 0xE1
#define PWCTR6  0xFC

#define WIDTH  128
#define HEIGHT 160

unsigned char write_spi(unsigned char data);
void write_data(unsigned char data);
void write_command(unsigned char command);
void set_addr(unsigned char xStart,unsigned char yStart,unsigned char xEnd,unsigned char yEnd);
void set_color(unsigned int color);
void fill_screen(unsigned int color);
void write_config(unsigned char command, unsigned char numberOfArgs, const char *args);
void write_config_with_delay(unsigned char command, unsigned int ms);
void Init_LCD();
void drawChar1(unsigned char x, unsigned char y,unsigned char c,unsigned int color); 
void drawString1(unsigned char x, unsigned char y, char *string, unsigned int color);
void drawChar2(unsigned char x, unsigned char y, char c,unsigned int color);
void drawString2(unsigned char x, unsigned char y, char *string,unsigned int color);
void drawChar3(unsigned char x, unsigned char y, unsigned char c, unsigned int color);
void drawString3(unsigned char x, unsigned char y, char *string, unsigned int color);
extern char *Itoa(int32_t Value, uint8_t Base, uint8_t Digits);
extern void drawText(unsigned char x, unsigned char y,char *text,unsigned int color);
#endif /*ST7735_H_*/
