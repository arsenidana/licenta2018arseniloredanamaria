################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/ST7735.c \
../Src/adc.c \
../Src/dma.c \
../Src/esp8266.c \
../Src/freertos.c \
../Src/gpio.c \
../Src/main.c \
../Src/robot.c \
../Src/spi.c \
../Src/stm32f4xx_hal_msp.c \
../Src/stm32f4xx_hal_timebase_TIM.c \
../Src/stm32f4xx_it.c \
../Src/system_stm32f4xx.c \
../Src/tim.c \
../Src/usart.c 

OBJS += \
./Src/ST7735.o \
./Src/adc.o \
./Src/dma.o \
./Src/esp8266.o \
./Src/freertos.o \
./Src/gpio.o \
./Src/main.o \
./Src/robot.o \
./Src/spi.o \
./Src/stm32f4xx_hal_msp.o \
./Src/stm32f4xx_hal_timebase_TIM.o \
./Src/stm32f4xx_it.o \
./Src/system_stm32f4xx.o \
./Src/tim.o \
./Src/usart.o 

C_DEPS += \
./Src/ST7735.d \
./Src/adc.d \
./Src/dma.d \
./Src/esp8266.d \
./Src/freertos.d \
./Src/gpio.d \
./Src/main.d \
./Src/robot.d \
./Src/spi.d \
./Src/stm32f4xx_hal_msp.d \
./Src/stm32f4xx_hal_timebase_TIM.d \
./Src/stm32f4xx_it.d \
./Src/system_stm32f4xx.d \
./Src/tim.d \
./Src/usart.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F411xE -I"D:/STM32_IDE/workspace/MazeBot/Inc" -I"D:/STM32_IDE/workspace/MazeBot/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/STM32_IDE/workspace/MazeBot/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/STM32_IDE/workspace/MazeBot/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/STM32_IDE/workspace/MazeBot/Drivers/CMSIS/Include" -I"D:/STM32_IDE/workspace/MazeBot/Inc" -I"D:/STM32_IDE/workspace/MazeBot/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"D:/STM32_IDE/workspace/MazeBot/Middlewares/Third_Party/FreeRTOS/Source/include" -I"D:/STM32_IDE/workspace/MazeBot/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


