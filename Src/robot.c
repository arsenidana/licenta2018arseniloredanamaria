/*
 * robot.c
 */
#include"robot.h"
#include"stm32f4xx_hal.h"
#include"main.h"
#include"gpio.h"
#include"adc.h"
#include"tim.h"
#include"ST7735.h"
#include"esp8266.h"

TIM_OC_InitTypeDef ConfigLeftMotorSpeed;
TIM_OC_InitTypeDef ConfigRightMotorSpeed;

static const float Kp = 2.8;//3.6;//2.5;//3.5; //2;2.25;
static const float Ki = 0.05;//0.005;
static const float Kd = 0.1;//0.5;



static int32_t RobotGetPosition(uint32_t *S);
static int32_t RobotPIDAlg(uint32_t Setpoint, int32_t Position, uint8_t StartFlag);
static void RobotSetDirectionTurnRight(void);
static void RobotSetDirectionTurnLeft(void);
static void RobotSetDirectionForward(void);
static void RobotSetDirectionBackward(void);
static void RobotSetDirectionStop(void);
static void RobotSetLeftSpeed(uint32_t Speed);
static void RobotSetRightSpeed(uint32_t Speed);
static uint8_t RobotCheckCenter(uint32_t *sens);
static uint8_t RobotCheckOutside(uint32_t *sens);
static uint8_t RobotCheckLeft(uint32_t *sens);
static uint8_t RobotCheckRight(uint32_t *sens);
static uint8_t RobotCheckOnLine(uint32_t *sens);
static void RobotDoTurnLeft(void);
static void RobotDoTurnRight(void);
static void RobotDoTurnBack(void);


static void RobotStoreDirection(char p);

#define LINE_THRESHOLD			2000ul
#define THRESHOLD_OFFSET		1000ul
#define SETPOINT			0ul
#define ROBOT_SPEED			400ul

#define ON	1
#define OFF	0

/* Sensors data buffer*/
uint32_t Sensor[8];


static volatile uint8_t start_stop = 1;
static volatile uint8_t PIDOn = 1;
static volatile int32_t Integral;

volatile int32_t Command;
volatile uint32_t Robot_Position;
volatile uint8_t StatusOrngRobot;
//static volatile uint8_t TurnFlag = 0;
static volatile char path[100];      // array for storing direction of the robot on learning the path
static volatile uint8_t path_lenghth = 0; // the length of the path
static volatile uint32_t counter =0;


typedef struct _transitions
{
  uint8_t ActTurnLeft;
  uint8_t ActTurnRight;
  uint8_t ActTurnBack;
  uint8_t ActFinish;
  uint8_t RobotMode;
  uint8_t ChkTurnLeft;
  uint8_t ChkTurnRight;
  uint8_t ChkTurnBack;
  uint8_t ChkFinish;
}Transitions;

volatile Transitions Robot;

void RobotInitialization(void)
{
	Robot.ActFinish = OFF;
	Robot.ActTurnBack = OFF;
	Robot.ActTurnLeft = OFF;
	Robot.ActTurnRight = OFF;

	Robot.ChkTurnLeft = ON;
	Robot.ChkTurnRight = ON ;
	Robot.ChkTurnBack = ON;
	Robot.ChkFinish = ON;


	/* Configure initial motors speed */
	ConfigLeftMotorSpeed.OCMode = TIM_OCMODE_PWM1;
	ConfigLeftMotorSpeed.Pulse = 0;
	ConfigLeftMotorSpeed.OCNPolarity = TIM_OCPOLARITY_HIGH;
	ConfigLeftMotorSpeed.OCFastMode = TIM_OCFAST_DISABLE;

	ConfigRightMotorSpeed.OCMode = TIM_OCMODE_PWM1;
	ConfigRightMotorSpeed.Pulse = 0;
	ConfigRightMotorSpeed.OCNPolarity = TIM_OCPOLARITY_HIGH;
	ConfigRightMotorSpeed.OCFastMode = TIM_OCFAST_DISABLE;

	/* Turn on IR-LEDs and start acquisition */
	HAL_GPIO_WritePin(SensorsLeds_GPIO_Port,SensorsLeds_Pin,1);
	HAL_ADC_Start_DMA(&hadc1,&Sensor[0],8);

	/* Robot is moving forward */
	RobotSetDirectionForward();


	/* Start generating PWM */
	HAL_TIM_PWM_Start_IT(&htim2,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start_IT(&htim2,TIM_CHANNEL_2);
}

void RobotPIDLoop(void)
{
  int32_t SpeedMotorLeft;
  int32_t SpeedMotorRight;
  Robot_Position = RobotGetPosition(&Sensor[0]);
  Command = RobotPIDAlg(SETPOINT,Robot_Position,PIDOn);
  if(PIDOn == 1)
  {
    SpeedMotorLeft = ROBOT_SPEED + Command;
    SpeedMotorRight = ROBOT_SPEED - Command;
    if(SpeedMotorLeft > 1000)
    {
	SpeedMotorLeft = 1000;
    }
    if(SpeedMotorRight > 1000)
    {
	SpeedMotorRight = 1000;
    }
    if(SpeedMotorLeft < 0)
       {
   	SpeedMotorLeft = 0;
       }
       if(SpeedMotorRight < 0)
       {
   	SpeedMotorRight = 0;
       }

    RobotSetLeftSpeed(SpeedMotorLeft);
    RobotSetRightSpeed(SpeedMotorRight);
  }

}

void RobotDisplayData(void)
{
  counter++;
  drawString1(1, 20,"Position:",YELLOW);
  drawString1(62,20,Itoa(Robot_Position,10,6),GREEN);
 drawString1(1,40,"Command:",YELLOW);
  drawString1(62,40,Itoa(Command,10,6),GREEN);

  drawString1(1, 40,&path[0],GREEN);

  drawString1(62,60, Itoa(counter,10,6),GREEN);
 // drawString1(62, 60,Itoa(Sensor[1],10,6),GREEN);
 // drawString1(62, 80,Itoa(Sensor[2],10,6),GREEN);
 // drawString1(62, 100,Itoa(Sensor[3],10,6),GREEN);
 // drawString1(62, 120,Itoa(Sensor[4],10,6),GREEN);
 // drawString1(62, 140,Itoa(Sensor[5],10,6),GREEN);

}

void RobotControlAlgorithm(void)
{

  if((RobotCheckLeft(&Sensor[0]) == ON) && (Robot.ChkTurnLeft == 1))
    {
      Robot.ActTurnLeft = ON;
      RobotSetDirectionStop();
      PIDOn=0;
      RobotSetLeftSpeed(470);
      RobotSetRightSpeed(470);
      RobotSetDirectionTurnLeft();
      Robot.ChkTurnLeft = OFF;
    }

  if((RobotCheckOutside(&Sensor[0]) == ON) && (Robot.ChkTurnBack == ON))
    {
        Robot.ActTurnBack = ON;
        RobotSetDirectionStop();
        PIDOn=0;
        RobotSetLeftSpeed(470);
        RobotSetRightSpeed(470);
        RobotSetDirectionTurnLeft();
        Robot.ChkTurnBack = OFF;
    }

  if(Robot.ActTurnBack== ON)
  {
         RobotDoTurnBack();
  }
  if(Robot.ActTurnLeft== ON)
    {
       RobotDoTurnLeft();
    }



  /*if((RobotCheckLeft(&Sensor[0])== 1) && (TurnFlag == 0))
  {
        PIDOn=0;
        RobotSetDirectionStop();
        RobotSetLeftSpeed(570);
        RobotSetRightSpeed(490);
        RobotSetDirectionTurnLeft();
        TurnFlag=1;
  }

  if(Sensor[0]<800 && Sensor[1]<800)
  {

      if((RobotCheckCenter(&Sensor[0])==1) && (TurnFlag==1))
	{
	  RobotSetDirectionStop();
	  RobotSetLeftSpeed(0);
	  RobotSetRightSpeed(0);
	  TurnFlag = 0;
	  Integral = 0;
	  PIDOn = 1;
	  RobotSetDirectionForward();

	}
  }*/

}


static void RobotSetDirectionStop(void)
{
        RobotSetLeftSpeed(0);
        RobotSetRightSpeed(0);
	HAL_GPIO_WritePin(M_LEFT_FW_GPIO_Port,M_LEFT_FW_Pin,0);
	HAL_GPIO_WritePin(M_LEFT_BW_GPIO_Port,M_LEFT_BW_Pin,0);

	HAL_GPIO_WritePin(M_RIGHT_FW_GPIO_Port,M_RIGHT_FW_Pin,0);
	HAL_GPIO_WritePin(M_RIGHT_BW_GPIO_Port,M_RIGHT_BW_Pin,0);
}

static void RobotSetDirectionForward(void)
{
	HAL_GPIO_WritePin(M_LEFT_FW_GPIO_Port,M_LEFT_FW_Pin,1);
	HAL_GPIO_WritePin(M_LEFT_BW_GPIO_Port,M_LEFT_BW_Pin,0);

	HAL_GPIO_WritePin(M_RIGHT_FW_GPIO_Port,M_RIGHT_FW_Pin,1);
	HAL_GPIO_WritePin(M_RIGHT_BW_GPIO_Port,M_RIGHT_BW_Pin,0);
}

static void RobotSetDirectionBackward(void)
{
	HAL_GPIO_WritePin(M_LEFT_FW_GPIO_Port,M_LEFT_FW_Pin,0);
	HAL_GPIO_WritePin(M_LEFT_BW_GPIO_Port,M_LEFT_BW_Pin,1);

	HAL_GPIO_WritePin(M_RIGHT_FW_GPIO_Port,M_RIGHT_FW_Pin,0);
	HAL_GPIO_WritePin(M_RIGHT_BW_GPIO_Port,M_RIGHT_BW_Pin,1);
}

static void RobotSetDirectionTurnRight(void)
{
	HAL_GPIO_WritePin(M_LEFT_FW_GPIO_Port,M_LEFT_FW_Pin,1);
	HAL_GPIO_WritePin(M_LEFT_BW_GPIO_Port,M_LEFT_BW_Pin,0);
	HAL_GPIO_WritePin(M_RIGHT_FW_GPIO_Port,M_RIGHT_FW_Pin,0);
	HAL_GPIO_WritePin(M_RIGHT_BW_GPIO_Port,M_RIGHT_BW_Pin,1);
}
static void RobotSetDirectionTurnLeft(void)
{
	HAL_GPIO_WritePin(M_LEFT_FW_GPIO_Port,M_LEFT_FW_Pin,0);
	HAL_GPIO_WritePin(M_LEFT_BW_GPIO_Port,M_LEFT_BW_Pin,1);

	HAL_GPIO_WritePin(M_RIGHT_FW_GPIO_Port,M_RIGHT_FW_Pin,1);
	HAL_GPIO_WritePin(M_RIGHT_BW_GPIO_Port,M_RIGHT_BW_Pin,0);
}

static void RobotSetLeftSpeed(uint32_t Speed)
{
	ConfigLeftMotorSpeed.Pulse = Speed;
}

static void RobotSetRightSpeed(uint32_t Speed)
{
	ConfigRightMotorSpeed.Pulse = Speed;
}

/* Function implements PID algorithm control */
static int32_t RobotPIDAlg(uint32_t Setpoint, int32_t Position, uint8_t StartFlag)
{
	int32_t Error;
	static int32_t Prev_Error;

	int32_t Derivative;

	float Command = 0;
	if (StartFlag == 1)
	{
		/* Calculate position error */
		Error = Position;
		/* Calculate integral error */
		Integral += Error;
		/* Set HIGH limit of integral */
		if (Integral > 250)
		{
			Integral = 250;
		}
		else
		{
			/* Set LOW limit of integral */
			if (Integral < -250)
			{
				Integral = -250;
			}
		}
		/* Calculate the derivative */
		Derivative = Error - Prev_Error;
		/* Calculate the output command of PID */
		Command = (Kp * (float) Error) + (Ki * (float) Integral) + (Kd * (float) Derivative);
		/* Store the previous error */
		Prev_Error = Error;
	}
	else
	{
		Integral = 0;
		Prev_Error = 0;
	}

	/*Return command*/
	return (int32_t) (Command);
}

/* Start - Stop trigger function */
void RobotStartStop (void)
{
	start_stop ^= 1;
}

/* Function check if robot is outside of track */
static uint8_t RobotCheckOutside(uint32_t *sens)
{
  uint8_t Status=0;
    uint8_t idx;
    uint8_t ctr=0;
    for(idx=0;idx<8;idx++)
    	{
             if((sens[idx] < (LINE_THRESHOLD - THRESHOLD_OFFSET)))
               {
                 ctr++;
               }
    	}
    if(ctr == 8)
      {
        Status = 1;
      }

    return Status;
}



/* Function check if robot is on center of the line */
static uint8_t RobotCheckCenter(uint32_t *sens)
{
  uint8_t Status=0;
    if( ((sens[3] > (LINE_THRESHOLD - THRESHOLD_OFFSET)) && (sens[3] < (LINE_THRESHOLD + THRESHOLD_OFFSET))) ||
      ((sens[4] > (LINE_THRESHOLD - THRESHOLD_OFFSET)) && (sens[4] < (LINE_THRESHOLD + THRESHOLD_OFFSET))))
      {
	Status = 1;
      }

  return Status;
}


/* Function check if robot is with all sensors on line */
static uint8_t RobotCheckOnLine(uint32_t *sens)
{
  uint8_t ctr = 0;
  uint8_t Status=0;
  uint8_t idx = 0;
  for(idx=0; idx<8; idx++)
    {
      if(sens[idx] >(LINE_THRESHOLD - THRESHOLD_OFFSET) && sens[idx] < (LINE_THRESHOLD + THRESHOLD_OFFSET) )
	{ctr++;}
    }
  if(ctr == 8)
    {
      Status = 1;
    }

  return Status;
}

/* Function check if robot is on left intersection */
static uint8_t RobotCheckLeft(uint32_t *sens)
{

	uint8_t Status=0;
	if((sens[0] > (LINE_THRESHOLD - THRESHOLD_OFFSET)) && (sens[0] < (LINE_THRESHOLD + THRESHOLD_OFFSET))&& \
	    (sens[1] > (LINE_THRESHOLD - THRESHOLD_OFFSET)) && (sens[1] < (LINE_THRESHOLD + THRESHOLD_OFFSET)))
	{
		Status = 1;
	}

	return Status;
}

/* Function check if robot is on right intersection */
static uint8_t RobotCheckRight(uint32_t *sens)
{

	uint8_t Status=0;
	if((sens[6] > (LINE_THRESHOLD - THRESHOLD_OFFSET)) && (sens[6] < (LINE_THRESHOLD + THRESHOLD_OFFSET)) && \
	   (sens[7] > (LINE_THRESHOLD - THRESHOLD_OFFSET)) && (sens[7] < (LINE_THRESHOLD + THRESHOLD_OFFSET)))
	{
		Status = 1;
	}

	return Status;
}

 /* function for storing direction*/
 static void RobotStoreDirection(char p)
 {
   path[path_lenghth++] = p;
 }


/* Function calculates line position */
 static int32_t RobotGetPosition(uint32_t *S)
 {
   int32_t Position;
   int32_t SensorsAverage = 0;
   int32_t SensorsSum = 0;
   int8_t SensorIndex;
   for (SensorIndex = 0; SensorIndex < 8; SensorIndex++)
     {
       SensorsSum += S[SensorIndex];
     }

   SensorsAverage = (-40*(S[0]-S[7]))+(-30*(S[1]-S[6]))+(-20*(S[2]-S[5]))+(-10*(S[3]-S[4]));
   Position = (SensorsAverage / SensorsSum)*10;

   if(Position < -210)
     {
       Position = -210;
     }
   return (int32_t)Position;
 }

static void RobotDoTurnBack(void)
{
   if((RobotCheckCenter(&Sensor[0])==1) && (Robot.ActTurnBack==ON))
	{
	    RobotSetDirectionStop();
	    Robot.ActTurnBack = OFF;
	    RobotSetDirectionForward();
	    Robot.ChkTurnBack = ON;
	    RobotStoreDirection('B');
	    path[10]='\0';
	    PIDOn = 1;
	}

}

static void RobotDoTurnLeft(void)
{
   if(Sensor[0]<1000 && Sensor[1]<1000)
   {

       if((RobotCheckCenter(&Sensor[0])==1) && (Robot.ActTurnLeft==ON))
 	{
	  Robot.ActTurnLeft=OFF;
	  RobotSetDirectionStop();
	  RobotSetDirectionForward();
	  Robot.ChkTurnLeft = ON;
	  RobotStoreDirection('L');
	  path[10]='\0';
 	  PIDOn = 1;

         }
   }
}


/* Update motors speed */
void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim)
{
	HAL_TIM_PWM_Stop_IT(htim,TIM_CHANNEL_1);
	HAL_TIM_PWM_Stop_IT(htim,TIM_CHANNEL_2);
	HAL_TIM_PWM_ConfigChannel(&htim2, &ConfigLeftMotorSpeed, TIM_CHANNEL_1);
	HAL_TIM_PWM_ConfigChannel(&htim2, &ConfigRightMotorSpeed, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start_IT(&htim2,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start_IT(&htim2,TIM_CHANNEL_2);
}
