#include "ST7735.h"
#include "font_5x7.h"
#include "font_8x12.h"
#include "font_11x16.h"
#include "stm32f4xx_hal.h"
#include "main.h"
#include "spi.h"

static unsigned int BGCOLOR = BLACK;

void write_data(unsigned char data) 
{
	asm("nop");
	asm("nop");
	HAL_GPIO_WritePin(Display_DC_GPIO_Port,Display_DC_Pin,1);
	asm("nop");
	asm("nop");
	HAL_GPIO_WritePin(Display_CSE_GPIO_Port,Display_CSE_Pin,0);

	HAL_SPI_Transmit(&hspi2,&data,1,1);

	HAL_GPIO_WritePin(Display_CSE_GPIO_Port,Display_CSE_Pin,1);
}
void write_command(unsigned char command) 
{
	asm("nop");
	asm("nop");
	HAL_GPIO_WritePin(Display_DC_GPIO_Port,Display_DC_Pin,0);
	asm("nop");
	asm("nop");
	HAL_GPIO_WritePin(Display_CSE_GPIO_Port,Display_CSE_Pin,0);
	HAL_SPI_Transmit(&hspi2,&command,1,1);
	HAL_GPIO_WritePin(Display_CSE_GPIO_Port,Display_CSE_Pin,1);
}
void set_addr(unsigned char xStart,unsigned char yStart,unsigned char xEnd,unsigned char yEnd)
{
	write_command(CASET);
	write_data(0x00);
	write_data(xStart);
	write_data(0x00);
	write_data(xEnd);
	write_command(RASET);
	write_data(0x00);
	write_data(yStart);
	write_data(0x00);
	write_data(yEnd);
	write_command(RAMWR);
	// data to follow
}
void set_color(unsigned int color)
{
	unsigned char colorH = color >> 8;
	unsigned char colorL = color;
	write_data(colorH);
	write_data(colorL);
}
void fill_screen(unsigned int color) {

unsigned char colorH = color >> 8;
unsigned char colorL = color;
unsigned int dim=20480;

	set_addr(0, 0, 127, 159);

	while (dim > 0)
	{
    	write_data(colorH);
		write_data(colorL);
		dim--;
	}
}
void write_config(unsigned char command, unsigned char numberOfArgs, const char *args) {
	write_command(command);
	while (numberOfArgs--) {
		write_data(*args++);
	}
}
void write_config_with_delay(unsigned char command, unsigned int ms) {
	write_command(command);
	HAL_Delay(ms);
}

const char frmctr[] = { 0x01, 0x2C, 0x2D, 0x01, 0x2C, 0x2D };
const char invctr[] = { 0x07 };
const char pwctr1[] = { 0xA2, 0x02, 0x84 };
const char pwctr2[] = { 0xC5 };
const char pwctr3[] = { 0x0A, 0x00 };
const char pwctr4[] = { 0x8A, 0x2A };
const char pwctr5[] = { 0x8A, 0xEE };
const char vmctr1[] = { 0x0E };
const char madctl[] = { 0xC8 };
const char colmod[] = { 0x05 }; // 16-bit color

void Init_LCD() 
{
	HAL_GPIO_WritePin(Display_RST_GPIO_Port,Display_RST_Pin,0);
	HAL_Delay(200);
	HAL_GPIO_WritePin(Display_RST_GPIO_Port,Display_RST_Pin,1);
	write_config_with_delay(SLPIN, 150); // sleep out
	write_config_with_delay(SWRESET, 150); 
	write_config_with_delay(SLPOUT, 150);
	write_config(FRMCTR1, 3, frmctr); // frame rate in normal mode
	write_config(FRMCTR2, 3, frmctr); // frame rate in idle mode
	write_config(FRMCTR3, 6, frmctr); // frame rate in partial mode
	write_config(INVCTR, 1, invctr); // display inversion control
	write_config(PWCTR1, 3, pwctr1); // power control settings
	write_config(PWCTR2, 1, pwctr2); // power control settings
	write_config(PWCTR3, 2, pwctr3); // power control settings
	write_config(PWCTR4, 2, pwctr4); // power control settings
	write_config(PWCTR5, 2, pwctr5); // power control settings
	write_config(VMCTR1, 1, vmctr1); // power control settings
	write_config(INVOFF, 0, 0); // display inversion off
	write_config(MADCTL, 1, madctl); // memory data access control
	write_config(COLMOD, 1, colmod); // color mode
	write_config_with_delay(NORON, 10); // normal display on
	write_config_with_delay(DISPON, 120); // display on
}

void drawChar1(unsigned char x, unsigned char y,unsigned char c,unsigned int color) {
	unsigned char col = 0;
	unsigned char row = 0;
	unsigned char bt = 0x01;
	unsigned char oc = c - 0x20;
	set_addr(x, y, x + 4, y + 7); // if you want to fill column between chars, change x + 4 to x + 5
	while (row < 8) {
		while (col < 5) {
			if (font_1[oc][col] & bt) 
			{
				set_color(color);			
			} 
			else 
			{
				set_color(BGCOLOR);
			}
			col++;
		}
		// if you want to fill column between chars, writeData(bgColor) here
		col = 0;
		bt <<= 1;
		row++;
	}
}
void drawString1(unsigned char x, unsigned char y, char *string, unsigned int color)
{
	unsigned char xs = x;
	while (*string) {
		drawChar1(xs, y, *string++,color);
		xs += 6;
	}
}

void drawText(unsigned char x, unsigned char y,char *text,unsigned int color)
{
	unsigned char xt=x,yt=y;
	while(*text)
	{
		if(*text=='\r' || *text=='\n')
		{
			yt+=8;
			xt=x;
			text++;
		}
		else
		{
			drawChar1(xt,yt,*text++,color);
			xt += 6;
			if(xt>=120)
			{
				yt+=8;
				xt=x;
				if(yt>=260)
				{
					yt=y;
				}
			}
		}

	}
}

void drawChar2(unsigned char x, unsigned char y, char c,unsigned int color)
{
	unsigned char col = 0;
	unsigned char row = 0;
	unsigned char bt = 0x80;
	unsigned char oc = c - 0x20;
	set_addr(x, y, x + 7, y + 11);
	while (row < 12) {
		while (col < 8) {
			if (font_2[oc][row] & bt) {
				//foreground
				set_color(color);
			} else {
				//background
				set_color(BGCOLOR);
			}
			bt >>= 1;
			col++;
		}
		bt = 0x80;
		col = 0;
		row++;
	}

}

void drawString2(unsigned char x, unsigned char y, char *string,unsigned int color)
{
	unsigned char xs = x;
	while (*string) {
		drawChar2(xs, y, *string++,color);
		xs += 8;
	}
}


void drawChar3(unsigned char x, unsigned char y, unsigned char c, unsigned int color) 
{
	unsigned char col = 0;
	unsigned char row = 0;
	unsigned int bt = 0x0001;
	unsigned char oc = c - 0x20;
	set_addr(x, y, x + 10, y + 15);
	while (row < 16) {
		while (col < 11) {
			if (font_3[oc][col] & bt) 
			{
				//foreground
			set_color(color);
			} 
			else 
			{
				//background
			set_color(BGCOLOR);
			}
			col++;
		}
		col = 0;
		bt <<= 1;
		row++;
	}
}

void drawString3(unsigned char x, unsigned char y, char *string, unsigned int color)
{
	unsigned char xs = x;
	while (*string) {
		drawChar3(xs, y, *string++,color);
		xs += 12;
	}
}

char *Itoa(int32_t Value, uint8_t Base, uint8_t Digits)
{
  static char Text_Buff[32];
  static char Sir[16] = "0123456789ABCDEF";
  char *Text_Address;
  unsigned char idx = 30;
  unsigned char numbers = 0;
  Text_Buff[31] = '\0';
  if (Value > 0)
  {
    while (Value > 0)
    {
      Text_Buff[idx] = Sir[Value % Base];
      Value = Value / Base;
      idx--;
      numbers++;
    }
    while ((Digits - numbers) > 0)
    {
      Text_Buff[idx] = ' ';
      idx--;
      Digits--;
    }
    Text_Address = &Text_Buff[idx + 1];
  }
  else
  {
    if (Value < 0)
    {
      while (Value < 0)
      {
        Text_Buff[idx] = Sir[((~Value) + 1) % Base];
        Value = Value / Base;
        idx--;
        numbers++;
      }
      if (Base == 10)
      {
        Text_Buff[idx] = '-';
        while ((Digits - (numbers + 1)) > 0)
        {
          idx--;
          Text_Buff[idx] = ' ';
          Digits--;
        }
        Text_Address = &Text_Buff[idx];
      }
      else
      {
        while ((Digits - numbers ) > 0)
        {
          Text_Buff[idx] = ' ';
          idx--;
          Digits--;
        }
        Text_Address = &Text_Buff[idx+1];
      }


    }
    else
    {
      Text_Buff[idx] = '0';
      while (--Digits)
      {
        idx--;
        Text_Buff[idx] = ' ';
      }
      Text_Address = &Text_Buff[idx];
    }
  }
  return Text_Address;
}
